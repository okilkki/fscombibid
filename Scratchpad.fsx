// #r "nuget: Flips"
#I "FsCombibid/bin/Debug/net5.0/"
#r "FsCombibid.dll"

open FsCombibid
open FsCombibid.Market

(**
# test
*)

[<Measure>] type kWh
[<Measure>] type EUR
[<Measure>] type Unit

type Commodity =
    | Electricity
    | Heat

type Parameter =
    | Location of Location
    | Generation of Generation

and Generation =
    | Solar
    | Diesel

and Location =
    | Community1
    | Community2


let s = {Id= "S"; Quantity= 1.5<kWh>; Cost= -2.<EUR>}
let d1 = {Id= "D1"; Quantity= -1.<kWh>; Cost= 2.<EUR>}
let d2 = {Id= "D2"; Quantity= -1.<kWh>; Cost= 3.<EUR>}
let d3 = {Id= "D3"; Quantity= -1.<kWh>; Cost= 4.<EUR>}


let bids = [
    ComplexBid(s, parameters=[Generation Solar; Location Community1], requirements=[])
    ComplexBid(d1, parameters=[Location Community2], requirements=[Location Community1])
    ComplexBid(d2, parameters=[Location Community2], requirements=[Location Community1])
    ComplexBid(d3, parameters=[], requirements=[Location Community2])
]

clear bids

