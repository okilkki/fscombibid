module FsCombibid.Allocation


open Flips
open Flips.Types
open Flips.SliceMap
open Flips.UnitsOfMeasure
open Flips.UnitsOfMeasure.Types


let internal minAccept = 1e-3

/// Solve default social cost minimizing allocation for seq of bids and pools
/// where they can trade in.
let allocate (bids: (PoolBid<'q,'c,'p> seq) * (Bundle<AtomicBid<'q,'c>,'c> seq)) =
    let bidPools, bundles = bids
    let poolsOf = SMap.ofSeq bidPools
    let bids = poolsOf.Keys

    // bid acceptance ratio decisions
    let bidRatios =
        DecisionBuilder<1> "BidRatio" {
            for b in bids do
            for p in poolsOf.[b] ->
                Continuous (0.0, 1.0)
        } |> SMap2.ofSeq
    
    // let pools = bidRatios.Keys2

    // bid quantities SliceMap
    let bidQuantities =
        [for b in bids do
            b, b.Quantity
        ] |> SMap.ofList
    
    // bid costs SliceMap
    let bidValuations =
        [for b in bids do
         for p in poolsOf.[b] ->
            (b,p), b.Cost
        ] |> SMap2.ofList
    
    // bundles
    let bundleOperators = bundles |> Seq.collect operatorsInBundle
    let bidsInBundles = bundleOperators |> Seq.collect bidsInBundle |> Seq.map BidOperand
    let allBundles = Seq.concat [bundleOperators; bidsInBundles]

    let bundleRatios =
        DecisionBuilder<1> "BundleRatio" {
            for b in allBundles ->
                Continuous (0.0, 1.0)
        } |> SMap.ofSeq

    let bundleValuations = 
        [for b in bundleOperators do 
            b, valuationOf b
        ] |> SMap.ofList
    
    let binAct = 
        DecisionBuilder<1> "BundleBinaryActivation" {
            for b in allBundles ->
                Boolean
        } |> SMap.ofSeq


    // objective
    let obj = sum (bidRatios .* bidValuations) + sum (bundleRatios .* bundleValuations)
    let objective = Objective.create "NetBenefit" Maximize obj


    // constraints
    let balanceConstraints =
        ConstraintBuilder "MarketTotal" {
        for p in bidRatios.Keys2 ->
            sum (bidQuantities .* bidRatios.[All, p]) == LanguagePrimitives.FloatWithMeasure<'q> 0.0
        }
    
    let bidTotalConstraints =
        ConstraintBuilder "BidTotal" {
        for b in bidRatios.Keys1 ->
            sum bidRatios.[b, All] <== 1.0
        }
        

    let rec bundleConstraintsBinary (bn: Bundle<AtomicBid<'q,'c>,'c>) =
        seq {
            // Binary variable activated if and only if activation ratio > `minAccept`
            yield Constraint.create $"Binary up {bn}"   (bundleRatios.[bn] <== binAct.[bn])
            yield Constraint.create $"Binary low {bn}"  (bundleRatios.[bn] >== minAccept * binAct.[bn])
            
            match bn with
            | CombinationOperator(op,bs,_) ->
                yield! bs |> Seq.collect bundleConstraintsBinary
                let binTotal = sum binAct.[In (Set.ofList bs)]
                let total = sum bundleRatios.[In (Set.ofList bs)]

                match bn with
                | And _ ->
                    // All have to be accepted or none
                    yield Constraint.create $"AND all {bn}" ((float bs.Length) * binAct.[bn] == binTotal)
                | Or _ ->
                    // If at least one accepted, operator activated
                    yield Constraint.create $"OR one {bn}" (binTotal <== (float bs.Length) * binAct.[bn])
                    // If none accepted, operator not activated
                    yield Constraint.create $"OR none {bn}" (binAct.[bn] <== binTotal)
                    // Total of operands (activations) should be smaller than activation
                    yield Constraint.create $"OR tot {bn}" (bundleRatios.[bn] >== total)

                | Xor _ ->
                    // At most one can be accepted
                    yield Constraint.create $"XOR {bn}" (binAct.[bn] == binTotal)
                    // Total of operands (activations) should be smaller than activation
                    yield Constraint.create $"XOR tot {bn}" (bundleRatios.[bn] >== total)
                | _ -> yield! []
            
            | _ -> yield! []
        }


    let rec bundleConstraints (bn: Bundle<AtomicBid<'q,'c>,'c>) =
        seq {
            match bn with
            | CombinationOperator(op,bs,v) ->
                yield! if v = None then bn |> bundleConstraintsBinary else Seq.empty
                yield! bs |> Seq.collect bundleConstraints
                
                match bn with
                | And _ ->
                    yield! bs |> Seq.map (fun b ->
                        Constraint.create $"AND {b}" (bundleRatios.[bn] <== bundleRatios.[b])
                    )
                | Or _ | Xor _ ->
                    let tot = sum bundleRatios.[In (Set.ofList bs)]
                    // Total acceptance of operands is limited by activation of operator (sum at most 1.0)
                    yield Constraint.create $"OR/XOR {bn}" (bundleRatios.[bn] <== tot)


                | _ -> yield! []

            | BidOperand b ->
                yield Constraint.create $"BidRatioInBundle {bn}" (bundleRatios.[bn] == sum bidRatios.[b, All])
            | _ -> yield! []
        }

    let bundleConstraints =
        bundles
        |> Seq.collect bundleConstraints
        |> Seq.distinctBy (fun c -> c.Name)

    // set up model and solve    
    let model =
        Model.create objective
        |> Model.addConstraints balanceConstraints
        |> Model.addConstraints bidTotalConstraints
        |> Model.addConstraints bundleConstraints


    let solverSettings = {
        SolverType = SolverType.CBC
        MaxDuration = 10_000L
        WriteLPFile = None
        WriteMPSFile = None
    }

    let solverResult = Solver.solve solverSettings model

    // map results
    match solverResult with
    | Optimal solution ->
        {
            AllocationRatios = Solution.getValues solution bidRatios
        }
    
    // TODO: what to do when no solution?
    | _ -> {AllocationRatios = Map.empty}



