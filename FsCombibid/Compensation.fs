module FsCombibid.Compensation

open Flips
open Flips.Types
open Flips.SliceMap


/// Construct `ClearingResult` from accepted quantities and costs of each per `Pool`.
let clearingResult tradedByPool =
    let clearedBids =
        tradedByPool 
        |> Seq.groupBy (fun ((b,p),r) -> b)
        |> Seq.map (fun (b,vals) ->
            b, vals |> Seq.sumBy snd
        )

    {
        ClearedBids = clearedBids |> Map.ofSeq
        TradedByPool = tradedByPool |> Map.ofSeq
    }
    

/// Pay-as-bid compensation: assign accepted cost based on acceptance ratio.
let payAsBid allocation =
    allocation.AllocationRatios
    |> Map.toSeq
    |> Seq.map (fun ((b,p),r) -> (b.Id,p), r*b)
    |> clearingResult


// TODO
// let payAsClear allocation
