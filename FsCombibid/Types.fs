namespace FsCombibid


type BidId = string

/// Basic information required for bidding in auctions for an atomic bid
type AtomicBid<[<Measure>]'quantity, [<Measure>]'currency> = {
    Id: BidId
    Quantity: float<'quantity>
    Cost: float<'currency>
} with
    member this.Price = - this.Cost / this.Quantity

    member this.IsSupply = this.Quantity >= LanguagePrimitives.FloatWithMeasure<'quantity> 0.0

    static member Zero = {
        Id=""
        Quantity=LanguagePrimitives.FloatWithMeasure<'quantity> 0.0
        Cost=LanguagePrimitives.FloatWithMeasure<'currency> 0.0
    }

/// Different matchable bid types
type Bid<[<Measure>]'q,[<Measure>]'c,'p when 'p : comparison> = 
    | AtomicBid of AtomicBid<'q,'c>
    | ComplexBid of AtomicBid<'q,'c> * parameters: Set<'p> * requirements: Set<'p>
    | PoolBid of PoolBid<'q,'c,'p>
    | Bundle of Bundle<Bid<'q,'c,'p>, 'c>


and PoolBid<[<Measure>]'q,[<Measure>]'c,'p when 'p : comparison> =
    AtomicBid<'q,'c> * Set<Pool<'p>>

/// Pools where bids can trade in
and Pool<'p when 'p: comparison> =
    | DefaultPool
    | Market of 'p
    | P2P of BidId * BidId

/// Bid combinations (bundles) can consist of operators or operands.
and Bundle<'b,[<Measure>]'c> =
    | BidOperand of 'b
    | And of BundleValues<'b,'c>
    | Or of BundleValues<'b,'c>
    | Xor of BundleValues<'b,'c>

/// Bid operators consist of a list of bundles and an optional valuation.
and BundleValues<'b,[<Measure>]'c> =
    list<Bundle<'b,'c>> * (float<'c> option)

/// The result of the allocation consists of rations per pool traded for each bid.
type AuctionAllocation<[<Measure>]'q,[<Measure>]'c,'p when 'p: comparison> = {
    AllocationRatios: Map<AtomicBid<'q,'c> * Pool<'p>, float>
    // PoolPrices: Map<Pool<'q,'c,'p>, float<'c/'q>>  // TODO flips 3.0
}

/// Final clearing result consists of cleared bids (quantity and cost) in total
/// and per pool.
type MarketClearing<[<Measure>]'q, [<Measure>]'c,'p when 'p: comparison> = {
    ClearedBids: Map<BidId, AtomicBid<'q,'c>>
    TradedByPool: Map<BidId * Pool<'p>, AtomicBid<'q,'c>>
    // PoolPrices: Map<Pool<'q,'c,'p>, float<'c/'q>>  // TODO flips 3.0
} 



[<AutoOpen>]
module OperatorOperations = 
    let (|CombinationOperator|_|) op =
        match op with
        | Xor(bs,v) | And(bs,v) | Or(bs,v) -> Some(box op, bs, v)
        | _ -> None

    let CombinationOperator (op, bs, v) =
        match unbox op with
        | Xor(bs,_) -> Xor(bs,v)
        | Or(bs,_) -> Or(bs,v)
        | And(bs,_) -> And(bs,v)
        | _ -> invalidArg "" $"Invalid CombinationOperator {op}"

    let sumOptions (v1: float<'c> option) (v2: float<'c> option) = 
        match v1,v2 with
        | Some v, None | None, Some v -> Some v
        | Some v1, Some v2 -> Some (v1+v2)
        | None, None -> None


type AtomicBid<[<Measure>]'q, [<Measure>]'c>
with
    static member (+) (lhs: AtomicBid<'q,'c>, rhs: AtomicBid<'q,'c>) =
        if lhs = AtomicBid.Zero then
            rhs
        else
            {
                Id=lhs.Id
                Quantity=lhs.Quantity+rhs.Quantity
                Cost=lhs.Cost+rhs.Cost
            }
    
    static member (*) (r: float, b: AtomicBid<'q,'c>) =
        {b with Quantity=r*b.Quantity; Cost=r*b.Cost}
    
    static member (+) ((b: AtomicBid<'q,'c>), (v: float<'c>)) =
        {b with Cost=b.Cost+v}
 

type Bid<[<Measure>]'q,[<Measure>]'c,'p when 'p : comparison>
with
    /// Add valuation to bid or bundle
     static member (+) ((b: Bid<'q,'c,'p>), (v: float<'c>)) =
        match b with
        | AtomicBid a -> AtomicBid a+v
        | ComplexBid(a,p,r) -> ComplexBid(a+v,p,r)
        | PoolBid(a,p) -> PoolBid(a+v,p)
        | Bundle bn ->
            match bn with
            | BidOperand b -> Bundle(BidOperand(b+v))
            | CombinationOperator(op,bs,v1) -> 
                let nv = defaultArg v1 (LanguagePrimitives.FloatWithMeasure<'c> 0.0) + v
                Bundle(CombinationOperator(op, bs, Some nv))
            | _ -> invalidArg "" $"Invalid CombinationOperator {bn}"


type Bundle<'b,[<Measure>]'c>
with
    /// Overload for `And` with two bundles
    static member (&!) ((lhs: Bundle<'b,'c>), (rhs: Bundle<'b,'c>)) =
        match lhs with
            | BidOperand b1 ->
                match rhs with
                    // Append to existing rhs bundle with same operator (And)
                    | And(bs,v) -> And([lhs] @ bs, v)
                    // In all other cases, hierarchically combine
                    | _ -> And([lhs; rhs], None)
            | And(bs,v1) ->
                match rhs with
                    // Append to existing lhs bundle with same operator (And)
                    | BidOperand b2 -> And(bs @ [rhs], v1)
                    // Append existing bundles with same operators (And) and combine valuations
                    | And(bs2,v2) -> And(bs @ bs2, sumOptions v1 v2)
                    // In all other cases, hierarchically combine
                    | _ -> And([lhs; rhs], None)
            | _ ->
                match rhs with
                    // In all other cases, hierarchically combine
                    | _ -> And([lhs; rhs], None)

    /// Overload for `And` with bundle and bid
    static member (&!) ((lhs: Bundle<Bid<'q,'c,'p>,'c>), (b: Bid<'q,'c,'p>)) =
            lhs &! BidOperand b
    

    /// Overload for `Or` with two bundles
    static member (|!) ((lhs: Bundle<'b,'c>), (rhs: Bundle<'b,'c>)) =
        match lhs with
            | BidOperand b1 ->
                match rhs with
                    // Append to existing rhs bundle with same operator (Or)
                    | Or(bs,v) -> Or([lhs] @ bs, v)
                    // In all other cases, hierarchically combine
                    | _ -> Or([lhs; rhs], None)
            | Or(bs,v1) ->
                match rhs with
                    // Append to existing lhs bundle with same operator (Or)
                    | BidOperand b2 -> Or(bs @ [rhs], v1)
                    // Append existing bundles with same operators (Or) and combine valuations
                    | Or(bs2,v2) -> Or(bs @ bs2, sumOptions v1 v2)
                    // In all other cases, hierarchically combine
                    | _ -> Or([lhs; rhs], None)
            | _ ->
                match rhs with
                    // In all other cases, hierarchically combine
                    | _ -> Or([lhs; rhs], None)

    /// Overload for `Or` with bundle and bid
    static member (|!) ((lhs: Bundle<Bid<'q,'c,'p>,'c>), (b: Bid<'q,'c,'p>)) =
            lhs &! BidOperand b

    
    /// Overload for `Xor` for two bundles
    static member (^!) ((lhs: Bundle<'b,'c>), (rhs: Bundle<'b,'c>)) =
        match lhs with
            | BidOperand b1 ->
                match rhs with
                    | BidOperand b2 -> Xor([BidOperand b1; BidOperand b2], None)
                    | Xor(bs,v) -> Xor(bs @ [BidOperand b1], None)
                    | _ -> Xor([BidOperand b1; rhs], None)
            | Xor(bs,v1) ->
                match rhs with
                    | BidOperand b2 -> Xor(bs @ [BidOperand b2], None)
                    | Xor(bs2,v2) -> Xor(bs @ bs2, None)
                    | _ -> Xor([lhs; rhs], None)
            | _ ->
                match rhs with
                    | _ -> Xor([lhs; rhs], None)

    /// Overload for `Xor` for bundle and bid
    static member (^!) ((lhs: Bundle<Bid<'q,'c,'p>,'c>), (b: Bid<'q,'c,'p>)) =
            lhs ^! BidOperand b


// Overloads are added also to bid type directly
type Bid<[<Measure>]'q,[<Measure>]'c,'p when 'p : comparison>
with
    static member (&!) ((lhs: Bid<'q,'c,'p>), (rhs: Bid<'q,'c,'p>)) =
        ((BidOperand lhs) &! (BidOperand rhs))
    
    static member (|!) ((lhs: Bid<'q,'c,'p>), (rhs: Bid<'q,'c,'p>)) =
        ((BidOperand lhs) |! (BidOperand rhs))
    
    static member (^!) ((lhs: Bid<'q,'c,'p>), (rhs: Bid<'q,'c,'p>)) =
        ((BidOperand lhs) ^! (BidOperand rhs))
