namespace FsCombibid


[<AutoOpen>]
module Bids = 

    /// Extracts the atomic bid
    let (|SingleBid|_|) b =
        match b with
        | AtomicBid a
        | ComplexBid(a,_,_)
        | PoolBid(a,_)
            -> Some a
        | Bundle _ -> None


    /// Checks whether `bid` can trade with other bid (`withBid`), one-way
    let internal canTradeWith bid withBid =
        match bid with
        | AtomicBid _ -> true
        
        | ComplexBid(requirements=req) ->
            match withBid with
            | AtomicBid _ -> true
            | ComplexBid(parameters=param) ->
                Set.isSubset req param
            | _ -> false
        
        | PoolBid(_,ps) ->
            match withBid with
            | PoolBid(a,ps2) ->
                not (Set.isEmpty (Set.intersect ps ps2))
            | _ -> false
        
        | Bundle _ -> false


    /// Check whether bids (`b1` and `b2`) can trade with each other, i.e. when
    /// their requirements and parameters match or they have common explicit pools
    /// and they have opposite signs for quantities.
    let canTrade b1 b2 =
        (canTradeWith b1 b2) && (canTradeWith b2 b1)
        && match b1,b2 with
            | SingleBid a1, SingleBid a2 -> sign a1.Quantity <> sign a2.Quantity
            | _ -> false


    /// Check whether `bid` can trade in a given pool
    let canTradeIn bid = function
        | DefaultPool
            -> match bid with
                | AtomicBid _ -> true
                | ComplexBid(_,_,req) -> req.IsEmpty
                | _ -> false

        | Market m as pool
            -> match bid with
                | ComplexBid (a,param,req) -> Set.contains m param && Set.contains m req
                | PoolBid(a,ps) -> ps |> Seq.contains pool
                | _ -> false
        
        | P2P(b1,b2)
            -> match bid with
                | SingleBid a -> a.Id = b1 || a.Id = b2
                | _ -> false

    /// Valuation of bid bundle or cost of bid
    let valuationOf = function
        | BidOperand b -> b.Cost
        | CombinationOperator (_,_,v) -> defaultArg v (LanguagePrimitives.FloatWithMeasure<'c> 0.0)
        | _ -> LanguagePrimitives.FloatWithMeasure<'c> 0.0


    let rec bidsInBundle c =
        seq {
            match c with
            | BidOperand b -> yield b
            | CombinationOperator(_,bs,_)
                -> for b in bs do yield! bidsInBundle b
            | _ -> yield! []
        }
    
    let rec operatorsInBundle c =
        seq {
            match c with
            | CombinationOperator(_,bs,_) ->
                yield c
                for b in bs do yield! operatorsInBundle b
            | _ -> yield! []
        }
    
    let rec asAtomicBundle = function
        | BidOperand b
            -> match b with
                | AtomicBid a | ComplexBid(a,_,_) | PoolBid(a,_)
                    -> BidOperand a
                | Bundle c
                    -> asAtomicBundle c
        | CombinationOperator(_,bs,v) as c ->
            let bsA = bs |> List.map asAtomicBundle
            match c with
                | Xor _ -> Xor(bsA,v)
                | Or _ -> Or(bsA,v)
                | And _ -> And(bsA,v)
                | _ -> invalidArg "" ""
        | _ -> invalidArg "" ""
            

[<AutoOpen>]
module Results = 
    let bidCleared (clearing: MarketClearing<'q, 'c, 'p>) (bidId: BidId) =
        defaultArg (Map.tryFind bidId clearing.ClearedBids) AtomicBid.Zero

    let describeResults clearing =
        printfn "---"
        clearing.ClearedBids |>
            Map.iter (fun b r ->
                printf $"{b}: Quantity=% .1f{r.Quantity} kWh"
                printfn $"    Price=% .1f{r.Price} EUR/KWh"
            )

