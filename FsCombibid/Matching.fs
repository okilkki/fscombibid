module FsCombibid.Matching


/// All combinations of length `n` for list elements in `l`
let rec internal comb n l = 
    match n, l with
    | 0, _ -> [[]]
    | _, [] -> []
    | k, (x::xs) -> List.map ((@) [x]) (comb (k-1) xs) @ comb k xs


/// All atomic bids in bundles
let allBidsFromSeq bids =
    seq {
        for b in bids do
            match b with
            | Bundle c -> yield! bidsInBundle c
            | _ -> b
    }


/// All bundles in seq of `Bid`s
let bundlesFromBids bids =
    bids
    |> Seq.choose (function
        | Bundle bs ->
            bs |> asAtomicBundle |> Some
        | _ -> None
    )


/// Assign `bids` to pools: 
///   - `AtomicBid`s to `DefaultPool`.
///   - `PoolBid`s to specified `Pool`s.
///   - `ComplexBid`s to `P2P` matches in case they can trade.
let assignToPools (bids: Bid<'q,'c,'p> seq) =
    let allBids = bids |> allBidsFromSeq |> Seq.toList

    let pools =
        allBids
        |> List.choose (fun b ->
            match b with
            | AtomicBid _ -> Some [DefaultPool]
            | PoolBid(_,ps) -> ps |> Set.toList |> Some
            | ComplexBid(a,_,_) ->
                allBids
                |> List.choose (fun b2 ->
                    if canTrade b b2 then
                        match b2 with
                        | SingleBid a2 when sign a.Quantity > 0 -> Some(P2P(a.Id,a2.Id))
                        | SingleBid a2 -> Some(P2P(a2.Id,a.Id))
                        | _ -> None
                    else None
                ) |> Some
            | _ -> None
        ) |> List.concat
        |> List.distinct

    let poolBids =
        allBids
        |> Seq.choose (fun b -> 
            match b with
            | SingleBid a -> Some (a, pools |> Seq.filter (canTradeIn b) |> Set.ofSeq)
            | _ -> None)

    let combinations = bundlesFromBids bids

    poolBids, combinations

/// Assigne all bids to `DefaultPool`
let assignToSinglePool bids =
    let poolBids = 
        bids
        |> allBidsFromSeq
        |> Seq.choose (function
            | SingleBid a -> Some (a, set [DefaultPool])
            | _ -> None
        )
    
    let combinations = bundlesFromBids bids
    
    poolBids, combinations


// let assignP2P (bids: Bid<'q,'c,'p> seq) =
    // TODO?