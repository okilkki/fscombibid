module FsCombibid.Market

open FsCombibid.Matching
open FsCombibid.Allocation
open FsCombibid.Compensation


let defaultCompensator = payAsBid
let defaultMatcher = assignToPools


/// Clear all bids in a single pool
let clearSinglePool (bids: Bid<'q,'c,'p> seq) =
    bids
    |> assignToSinglePool
    |> allocate
    |> payAsBid


/// Clear all bids with the default matching (assignment to pools) and compensator 
/// (pay-as-bid) methods
let clear bids =
    bids
    |> defaultMatcher
    |> allocate
    |> defaultCompensator

