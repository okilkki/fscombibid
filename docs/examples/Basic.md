# Basic example trading on explicit pools and using bid combinations

The following listing describes a basic example for a case where energy can be traded in two different commodities `Electricity` and `Heat`. The bidder of bids `"A1"` and `"A2"` is willing to sell `1 kWh` of `Electricity` and procure `1 kWh` of `Heat`. The bidder indicates an additional valuation of up to `1.5 EUR` in the proportion that both of the bids are accepted (*i.e.* maximum of their individual acceptances) by defining a bid combination (`Bundle`) using the `And` operator (with overloaded operators `&!` and `+`). Bid `"B"` can supply a maximum of `2 kWh` on both the `Electricity` or the `Heat` market, while `"C"` is willing to procure on the `Electricity` market.

```fsharp
open FsCombibid

// Define units of measure
[<Measure>] type kWh
[<Measure>] type EUR

type Commodity =
    | Electricity
    | Heat

// Define bids
let a1 = PoolBid({Id= "A1"; Quantity= 1.<kWh>; Cost= -2.<EUR>}, [Market Electricity])
let a2 = PoolBid({Id= "A2"; Quantity= -1.<kWh>; Cost= 1.5<EUR>}, [Market Heat])
let b =  PoolBid({Id= "B"; Quantity= 2.<kWh>; Cost= -3.<EUR>}, [Market Heat; Market Electricity])
let c =  PoolBid({Id= "C"; Quantity= -1.<kWh>; Cost= 1.<EUR>}, [Market Electricity])

// Clear using default allocation and compensation (pay-as-bid)
[
    Bundle(a1 &! a2) + 1.5<EUR>
    b; c
] |> Market.clear
```

If not enough valuation given, nothing is traded as the requested compensations do not match.

```fsharp
let bids = [
    Bundle(a1 &! a2) + 0.5<EUR>
    b; c
]
```

Bid `"B"` could also alternatively be expressed with two bids combined using `Or` on the two markets

```fsharp
let b1 =  PoolBid({Id= "B1"; Quantity= 2.<kWh>; Cost= -3.<EUR>}, [Market Heat])
let b2 =  PoolBid({Id= "B2"; Quantity= 2.<kWh>; Cost= -3.<EUR>}, [Market Electricity])

Bundle(b1 |! b2)
```
