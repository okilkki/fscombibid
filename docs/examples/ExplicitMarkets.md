# Explicitly defined markets

At first, we open the `FsCombibid` library and define types we want to use for our market. We define units of measures for traded quantity (energy in this case) and the chosen currency. Also define `Commodity` type consisting of two options: `Electricity` and `Heat`.

```fsharp
open FsCombibid

[<Measure>] type kWh
[<Measure>] type EUR

type Commodity =
    | Electricity
    | Heat
```

Then we can define three bids `a`, `b` and `c` which are assigned markets such that `a` is willing to procure only on the `Electricity` market, while `b` could offer on both markets and `c` only on the `Heat` market. The bids are then cleared.

```fsharp
// Define bids
let a = {Id = "A"; Quantity = -1.0<kWh>; Cost = 2.0<EUR>}
let b = {Id = "B"; Quantity = 2.0<kWh>; Cost = -3.5<EUR>}
let c = {Id = "C"; Quantity = 2.0<kWh>; Cost = -3.0<EUR>}

// Assign to markets
let bids = [
    a, [Market Electricity]
    b, [Market Electricity; Market Heat]
    c, [Market Heat]
]

// Clear using default allocation and compensation (pay-as-bid)
let clearing = Market.clear bids
```

The clearing result can be inspected for example in the following way.

```fsharp
clearing.ClearedBids |>
    Map.iter (fun b r ->
        printf $"{b}: Quantity=% .1f{r.Quantity} kWh"
        printfn $"    Price=% .1f{r.Price} EUR/KWh"
    )
```

Which should then output:

```console
A: Quantity=-1.0 kWh    Price=-3.0 EUR/KWh                  
B: Quantity= 1.0 kWh    Price=-2.0 EUR/KWh                  
C: Quantity= 0.0 kWh    Price=NaN EUR/KWh 
```
