# P2P trading using parameters and requirements

At first, we open the `FsCombibid` library and define types we want to use for our market. We define units of measures for traded quantity (energy in this case) and the chosen currency. The parameters used to define the potential trading partners are defined in the `Parameter` type. `Generation` consists of two options: `Solar` and `Diesel`, while `Location` specifies either `Community1` or `Community2` as the location.

```fsharp
open FsCombibid

[<Measure>] type kWh
[<Measure>] type EUR

type Parameter =
    | Location of Location
    | Generation of Generation

and Generation =
    | Solar
    | Diesel

and Location =
    | Community1
    | Community2

```

```fsharp
let s = {Id= "S"; Quantity= 1.5<kWh>; Cost= -2.<EUR>}
let d1 = {Id= "D1"; Quantity= -1.<kWh>; Cost= 2.<EUR>}
let d2 = {Id= "D2"; Quantity= -1.<kWh>; Cost= 3.<EUR>}
let d3 = {Id= "D3"; Quantity= -1.<kWh>; Cost= 4.<EUR>}


let bids = [
    ComplexBid(s, parameters=[Generation Solar; Location Community1], requirements=[])
    ComplexBid(d1, parameters=[Location Community2], requirements=[Location Community1])
    ComplexBid(d2, parameters=[Location Community2], requirements=[Location Community1])
    ComplexBid(d3, parameters=[], requirements=[Location Community2])
]

clear bids
```
