(***condition:prepare***)
#r "nuget: Flips"

#I "../FsCombibid/bin/Debug/net5.0/"
#r "FsCombibid.dll"

open FsCombibid

(**
# Background

> The following is an excerpt / modification of the text available as a [public deliverable](http://dominoesproject.eu/wp-content/uploads/2020/10/D3.5_DOMINOES_Aggregation_based_DR_v1.0.pdf) in the DOMINOES project, which  has  received  funding  from  the  European  Union's  Horizon  2020  research  and innovation programme under Grant Agreement No. 771066. **Disclaimer:** The views expressed in this document are the sole responsibility of the authors and do not necessarily reflect the views or position of the European Commission or the Innovation and Network Executive Agency. Neither the authors nor the DOMINOES consortium are responsible for the use which might be made of the information contained in here.

This document describes implementation details and relevant background information for the market clearing mechanisms implemented in the `FsCombibid` library. At first, the general problem of combinatorial auctions is presented and options for linking bids in such markets are described. Then the matching of bids in `FsCombibid` is shown and the clearing process is detailed by formulating the allocation problem as a [MILP](https://en.wikipedia.org/wiki/Integer_programming) problem.

## Table of contents

- [Combinatorial auctions](#Combinatorial-auctions)
- [Matching](#Matching)
- [Bid linking](#Bid-linking)
  - [Bidding languages](#Bidding-languages)
  - [Bid linking in FsCombibid](#Bid-linking-in-FsCombibid)
- [Allocation](#Allocation)
  - [Optimisation problem](#Optimisation-problem)
  - [Example allocation](#Example-allocation)

## Combinatorial auctions

> Combinatorial auctions are those auctions in which bidders can place bids on combinations of items, called “packages,” rather than just individual items. *[Peter Cramton, et al.](http://cramton.umd.edu/ca-book/cramton-shoham-steinberg-combinatorial-auctions.pdf)*

In practice, there are many examples of such auctions, such as in energy markets, where complex bids are often required to describe physical and financial constraints as well as valuations on flexible resources. For example in Nord Pool, there are [complex order types](https://www.nordpoolgroup.com/trading/Day-ahead-trading/Order-types/) for linking bids between time periods or to exclusive groups.

In the following, we define two units of measure for trading energy (in `kWh`) with Euros. Using these measures, atomic supply bids `s1` and `s2` and demand bids `d1` and `d2` are defined.
*)

[<Measure>] type kWh
[<Measure>] type EUR

let s1 = { Id = "S1"; Quantity =  1.<kWh>; Cost = -2.<EUR> }
let s2 = { Id = "S2"; Quantity =  2.<kWh>; Cost = -3.<EUR> }
let d1 = { Id = "D1"; Quantity = -1.<kWh>; Cost = 1.5<EUR> }
let d2 = { Id = "D2"; Quantity = -1.<kWh>; Cost = 1.<EUR> }

(**
## Matching

The bids can be defined as targeting distinct commodities (pools / markets) in several different ways.

- **Single market**, where all bids participate in a single pool
- **Explicit pools**, where bids are explicitly assigned to one (or several) pools where they can trade
- **Complex bids / P2P matching**, where bids are matched directly with other bids using parameters and requirements

For example, matching can be facilitated by defining types that describe the different commodities that can be traded and then assigning them to specific `Market`s (Pools).
*)

type Commodity =
    | Electricity
    | Heat

let ps1 = PoolBid(s1, set [Market Electricity])
let ps2 = PoolBid(s2, set [Market Electricity; Market Heat])
let pd1 = PoolBid(d1, set [Market Heat])
let pd2 = PoolBid(d2, set [Market Electricity])

(**
Alternatively, when the bids define *requirements*, the bids with matching *parameters* (and *requirements*, vice versa, if any) are assumed perfect substitutes for the bidder, i.e. there is no difference in perceived value for the bidder. In this case, the matching results in `P2P` pools as seen in the output below.
*)

type Generation =
    | Solar
    | Diesel

type Location =
    | Community1
    | Community2

type Parameter =
    | Location of Location
    | Generation of Generation


[
    ComplexBid(s1, parameters = set [Generation Solar; Location Community1], requirements = set[])
    ComplexBid(s2, parameters = set [Location Community2], requirements = set [Location Community1])
    ComplexBid(d1, parameters = set [Location Community2], requirements = set [Location Community1])
    ComplexBid(d2, parameters = set[], requirements = set [Location Community2])
] |> Matching.assignToPools
(*** include-it ***)


(**
## Bid linking

Complex orders (valuations) are often described in markets using domain specific bidding constructs. Alternatively, combinations of valuations and items could be described using generic bidding languages.

### Bidding languages

Bidding languages *"allow the expression of complex utility functions in a natural and concise way", [Boutelier & Hoos](https://www.cs.toronto.edu/~cebly/Papers/bidlang.pdf)*. When bidding for (or with) multiple heterogenous items, there is frequently a need to valuations which are dependent on the acceptance of the different items. The different types of valuations can often be defined in terms of *complementarity* and *substititutability, [Kaleta](http://yadda.icm.edu.pl/yadda/element/bwmeta1.element.baztech-31ac734b-f587-4320-9d36-45cc9a035ba1/c/Kaleta.pdf)*. Complementarity of two items means that the perceived value of both of them combined is larger than or equal to the sum of the values of the items separately. Conversely, substitutable items are items of which the combined value is less or equal to the value of the items separately. These valuations can be expressed using general logical bidding languages, where bids are linked using logical formulas.

Logical bidding languages could be separated in to different families, consisting of languages which allow *logical combinations of goods*, *logical combinations of bundle bids* and *logical combinations of bids and goods*, *[Boutelier & Hoos](https://www.cs.toronto.edu/~cebly/Papers/bidlang.pdf)*. In the first family of languages, logical formulas are evaluated fully, the goods can be accepted only if the logical formula is evaluated as true and the associated price is the valuation if it is accepted. In the second family of languages, the bids (consisting of bundles of requested items) are combined using logical formulas and associated prices define additional value based on the evaluation of the formula. The final family of languages combines properties of the previous types. For auctions with continuous goods, the evaluation of the logical formulas have to be slightly modified from the discrete cases, *[Kaleta](http://yadda.icm.edu.pl/yadda/element/bwmeta1.element.baztech-31ac734b-f587-4320-9d36-45cc9a035ba1/c/Kaleta.pdf)*.

In the following tables, several examples are shown of the first two bidding language families using bids (`B1`, `B2` and `B3`) that define their matching criteria, quantity and associated cost. The first table shows bids linked using the first family of languages and the second table using the second family. Multiple of these linkings could be also hierarchically combined in a single expression, as shown in the following sections.

#### Languages where formulas are evaluated fully (binary)

| Bid expression        | Description |
| --------------        | ----------- |
| `(B1 AND B2)`         | Both `B1` and `B2` have to be accepted, or neither. |
| `(B1 OR B2)`          | The sum of acceptance ratios of `B1` and `B2` can be at most 1. |
| `(B1 XOR B2 XOR B3)`  | At most one of `B1`, `B2` or `B3` can be accepted (exclusive or). |

#### Languages where formulas indicate additional valuations

| Bid expression       |     Description |
| --------------       |     ----------- |
| `(B1 AND B2, 2.0)`   |     The minimum of the acceptance ratios is valued with `2.0`. For example if `B1` is accepted at a ratio of `0.5` and `B2` at a ratio of  `0.3`, the indicated additional value is `2.0 * min(0.5, 0.3) = 0.6`. |
| `(B1 OR B2, 2.0)`    |     The sum of the acceptance ratios is valued with `2.0`. For example if `B1` is accepted at a ratio of `0.5` and `B2` at a ratio of `0.3`, the indicated additional value is `2.0 * (0.5 + 0.3) = 1.6`. Sum of acceptance ratios is limited to `1.0`. |

### Bid linking in FsCombibid

The bidder can link their bids with the exposed bidding language in order to form complex constraints and valutions. Using the overloaded operator `&!` (AND) to add valuation of `1.5 EUR` to the proportion that both supply bid `ps1` and demand bid `pd1` are accepted.
*)

let ps1AndPd1 = Bundle(ps1 &! pd1) + 1.5<EUR>
(*** include-value: ps1AndPd1 ***)


(**

## Allocation

The allocation (winner determination) problem is then solved by formulating the problem as a MILP problem, where the social welfare of accepted bids is maximized. [Flips](https://github.com/fslaborg/flips) is used to construct and solve the constrained optimisation problem.

Potential problems of the proposed solution and [combinatorial](http://cramton.umd.edu/ca-book/cramton-shoham-steinberg-combinatorial-auctions.pdf) [auctions](https://www.pnas.org/content/pnas/100/19/11153.full.pdf) in general include limited privacy, liquidity and interpretability, abuse of market power in small areas, difficult bid formation (i.e. preference elicitation), suitable pricing and especially computational complexity.

### Optimisation problem

In the case of simple bids without preferences or linking of bids, a simple formulation for the allocation problem could be devised. In the simplest case, the problem corresponds with a single merit order pool, where the allocation and prices can be determined from the intersection of the supply and demand quantity-price curves.

In the more general case, the allocation can be formulated as maximization of social welfare, which is defined as the sum of the valuations of all accepted bids. The constraints limit the activation of the bids based on the aforementioned bid linking as well as the `Pool`s where the bids can trade in. The objective function of the problem can be defined as

$$ \max_{a_i, t_{i,j}} \sum_{i=1}^N{r_i c_i} + \sum_{op} r_{op}v_{op} $$

The bids $bid_i = (q_i, c_i, \mathbb{T}_i)$ are fully determined by their costs, quantity, matching pools. The variables, constants and constraints of the problem are listed in the following tables.

#### Constants

| Constant / set                         | Description |
| -------------------                    | ----------- |
| $i \in \mathbb{B} = \{1, \ldots, N\}$  | Set of bids with index $i$ |
| $j \in \mathbb{P} = \{1, \ldots, M\}$  | Set of pools with index $j$ |
| $\mathbb{P}_i \subset \mathbb{P}$      | Set of pools in which bid $i$ can trade in |
| $\mathbb{B}_{op} \subset \mathbb{B}$   | Bids (operands) in specific linking operator $op$ |
| $v_{op} \in \mathbb{R}$                | Valuation of (acceptance of) operator $op$ (optional) |
| $q_i \in \mathbb{R}$                   | Quantity of energy willing to supply towards (or get from) market |
| $c_i \in \mathbb{R}$                   | Cost of bid willing to pay towards (or wants to get from) market |
| $\underline{r} \in [0, 1]$             | Minimum acceptance ratio (small constant such as $10^{-4}$) |

#### Decision variables

| Variable                | Description |
| --------                | ----------- |
| $r_{i,j} \in [0, 1]$    | Acceptance ratio of bid $i$ in pool $j$ |
| $t_{i,j} \in [0, 1]$    | Traded quantity of bid $i$ in pool $j$ |
| $r_{op} \in [0, 1]$     | Acceptance / activation ratio of operator $op$ |

The following variables are used only if required due to their inclusion in a bid linking of the first family of bidding languages (i.e. without valuation, see above).

| Variable / constant     | Description |
| -------------------     | ----------- |
| $a_i \in \{0, 1\}$      | Acceptance of bid $i$ |
| $a_{op} \in \{0, 1\}$   | Acceptance / activation of operator $op$|

#### Constraints for trading

| Constraint                                                                    | Description |
| ------------------------                                                      | ----------- |
| $\sum_{i \ \text{s.t.} \ j \in \mathbb{P}_i} q_i r_{i,j} = 0 \quad \forall j\in\mathbb{P}$      | Trade reciprocity for pool $j$ |
| $\sum_{j \in \mathbb{P}_i} r_{i,j} \leq 1$                                    | Total acceptance of bid $i$ |
| $\underline{r} a_i \leq r_i \leq a_i$                                         | If accepted, minimum acceptance ratio of bid $i$ should be obeyed. Only included if required due to participation in *binary* operator. |

#### Constraints for all operators

| Operator(s) | Constraint                                                      | Description |
| ----------- | ------------                                                    | ----------- |
| AND         | $r_{op} \leq r_i \quad \forall i \in \mathbb{B}_{op}$           | Limit amount of total activation of operator at most to max of operands |
| OR, XOR     | $r_{op} \leq \sum_{i \in \mathbb{B}_{op}}r_i$                   | Limit activation of operator to at most sum of activations of operands |
| *all*       | $a_{op} \underline{r} \leq r_{op}$                              | Activation of operator only if activation ratio large enough. Only if part of *binary* operator. |
| *all*       | $a_{op} \geq r_{op}$                                            | Activation of operator if activation ratio larger than 0.  Only if part of *binary* operator. |

#### Constraints for operators without valuations (binary)

| Operator(s) | Constraint                                                          | Description |
| ----------- | ----------                                                          | ----------- |
| OR          | $\sum_{i \in \mathbb{B}_{op}}a_i \leq a_{op} \|\mathbb{B}_{op}\|$   | If at least one accepted, operator activated |
| OR          | $a_{op} \leq \sum_{i \in \mathbb{B}_{op}}a_i$                       | If none accepted, operator not activated |
| AND         | $\|\mathbb{B}_{op}\|a_{op} = \sum_{i \in \mathbb{B}_{op}}a_i$       | All have to be accepted, or none |
| XOR         | $a_{op} = \sum_{i \in \mathbb{B}_{op}}a_i$                          | At most one can be accepted |
| OR, XOR     | $\sum_{i \in \mathbb{B}_{op}}r_i \leq r_{op}$                       | Total acceptance of operands (bids) is limited by activation / acceptance of operator (sum is at most 1) |

### Example allocation

We can then allocate the previously defined bids using the aforementioned optimization.
*)

[
    ps1AndPd1
    ps2
    pd2
] |> Market.clear
(*** include-it ***)

(**
The result of the allocation and the default compensation (pay-as-bid) is that the bids `S1` and `D1` are accepted ub the `Heat` and `Electricity` pools, while if no additional valuation was given, no bids would be accepted due to the high offer prices:
*)

[
    ps1
    pd1
    ps2
    pd2
] |> Market.clear
(*** include-it ***)
