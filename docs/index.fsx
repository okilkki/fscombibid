(**
# FsCombibid documentation & examples

This site includes the documentation, examples and background information for the `FsCombibid` library, which enables the definition and solving of multi-commodity divisible-good markets.

- [Background](Background.html)
- Examples:
  - [Basic example](examples/Basic.html)
  - [Matching with explicitly defined markets](examples/ExplicitMarkets.html)
  - [P2P matching](examples/P2P.html)

*)

