# Combinatorial auctions for *F#*

Package for creating and solving *continuous commodity (e.g. energy) heterogeneous two-sided markets*. Supports *combinatorial bids* (offers and requests). Bids can define requirements towards potentially matching bids, creating *ad-hoc* products. In addition, generic *bidding language* is implemented which can be used to define for example exclusive bids, hierarchical activation, additional (super-additive) valuations or more complex behaviours.

Features (should at some point) include

- Matching of multi-commodity bids (using units of measure functionality of F#), based on parameters and requirements (generic types user-definable) or explicit markets
  - Pool-based trading
  - Peer-to-peer trading
- Bid bundles / combinations (combinatorial auction) for expressing *e.g.* multi-commodity valuations
  - Supported operators: `And`, `Xor` and `Or`
  - Operator overloading for ergonomics
- Allocation using social welfare maximization using [Flips](https://github.com/fslaborg/flips)
- Various compensation methods (*TODO*)

## Getting started

Documentation & background details hosted on [Gitlab pages](https://okilkki.gitlab.io/fscombibid/).

### Installation

    chmod a+x build.sh
    ./build.sh

### Example usage

The following listing describes a basic example for a case where energy can be traded in two different commodities `Electricity` and `Heat`. The bidder of bids "A1" and "A2" is willing to sell `1 kWh` of `Electricity` and procure `1 kWh` of `Heat`. The bidder indicates an additional valuation of up to `1.5 EUR` in the proportion that both of the bids are accepted (*i.e.* maximum of their individual acceptances) by defining a bid combination (`Bundle`) using the `And` operator (with overloaded operators `&!` and `+`). Bid "B" can supply a maximum of `2 kWh` on both the `Electricity` or the `Heat` market, while "C" is willing to procure on the `Electricity` market.

```fsharp
open FsCombibid

// Define units of measure
[<Measure>] type kWh
[<Measure>] type EUR

type Commodity =
    | Electricity
    | Heat

// Define bids
let a1 = PoolBid({Id= "A1"; Quantity= 1.<kWh>; Cost= -2.<EUR>}, [Market Electricity])
let a2 = PoolBid({Id= "A2"; Quantity= -1.<kWh>; Cost= 1.5<EUR>}, [Market Heat])
let b =  PoolBid({Id= "B"; Quantity= 2.<kWh>; Cost= -3.<EUR>}, [Market Heat; Market Electricity])
let c =  PoolBid({Id= "C"; Quantity= -1.<kWh>; Cost= 1.<EUR>}, [Market Electricity])

// Clear using default allocation and compensation (pay-as-bid)
[
    Bundle(a1 &! a2) + 1.5<EUR>
    b; c
] |> Market.clear
```

More details are found in [docs/examples](docs/examples/Basic.md).

### Tests

    dotnet run -p FsCombibid.Test

## Built with

- [Flips](https://github.com/fslaborg/flips)
- [FsLab-docs](https://github.com/fslaborg/docs-template) (documentation template)
- [Expecto](https://github.com/haf/expecto) (tests)

## TODO

- Testing!
- Compensation (waiting for Flips 3.0: [flips/#110](https://github.com/fslaborg/flips/issues/110))
- Documentation
  - Available features
  - More examples
- Command line
  - Argument parsing [docopt.fs](https://github.com/docopt/docopt.fs) or [Argu](http://fsprojects.github.io/Argu/)?
  - Input parsing [FsLex & FsYacc](http://fsprojects.github.io/FsLexYacc/) / manually?
- C# interface / api?

## Licence

**Don't actually use this!** It is not implemented in full and tested even less.
This is just a place for me to try out some stuff.

[FsLab-docs template](https://github.com/fslaborg/docs-template) under MIT license.
