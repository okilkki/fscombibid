open System
open FsCombibid


[<Measure>] type kWh
[<Measure>] type EUR


[<EntryPoint>]
let main argv =
    // TODO: actual parsing
    

    let a = AtomicBid {Id = "A"; Quantity = 2.0<kWh>; Cost = -1.0<EUR>}
    let a1 = AtomicBid {Id = "A1"; Quantity = 1.0<kWh>; Cost = -1.0<EUR>}
    let a2 = AtomicBid {Id = "A2"; Quantity = 1.0<kWh>; Cost = -3.0<EUR>}
    let b = AtomicBid {Id = "B"; Quantity = -1.0<kWh>; Cost = 1.0<EUR>}
    let c = AtomicBid {Id = "C"; Quantity = -1.0<kWh>; Cost = 1.0<EUR>}

    let bids = [
        // Bundle(a1 ^! a2)
        // Bundle(a2 &! b) + 4.<EUR>
        // Bundle(a2 &! b)
        Bundle(b |! c)
        a
        // a1; a2; b
    ]

    let result = Market.clear bids

    describeResults result

    0 // return an integer exit code