module Tests.Integration

open Expecto
open FsCombibid


[<Measure>] type kWh
[<Measure>] type EUR


type Commodity =
    | Electricity
    | Heat

type Parameter = 
    | Commodity of Commodity
    | Location of string


[<Tests>]
let singlePool =
    let a = AtomicBid {Id = "A"; Quantity = 1.0<kWh>; Cost = -1.0<EUR>}
    let b = {Id = "B"; Quantity = -2.0<kWh>; Cost = 3.0<EUR>}

    let clearWithBCost cost =
      let b = AtomicBid {b with Cost = 1.0<EUR>}
      Market.clearSinglePool [a; b]

    // TODO more cases
    testList "clearing" [
    testCase "should match" <| fun _ ->
        let result = clearWithBCost 3<EUR>
        Expect.isTrue true "TODO test here"
    ]
