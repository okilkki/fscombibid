module Tests.Matching

open Expecto
open FsCombibid


[<Measure>] type kWh
[<Measure>] type EUR


type Parameter =
    | Location of Location
    | Generation of Generation

and Generation =
    | Solar
    | Diesel

and Location =
    | Community1
    | Community2


[<Tests>]
let p2pMatching =
    let supply = {Id= "s"; Quantity= 1.<kWh>; Cost= -1.<EUR>}
    let demand = {Id= "d"; Quantity= -1.<kWh>; Cost= 1.<EUR>}

    let A1 = ComplexBid({supply with Id="A1"}, parameters=set[Generation Solar; Location Community1], requirements=set[])
    let A2 = ComplexBid({supply with Id="A2"}, parameters=set[Location Community1], requirements=set[Location Community2])
    let B = ComplexBid({demand with Id="B"}, parameters=set[Location Community2], requirements=set[Location Community1])
    let C = ComplexBid({demand with Id="C"}, parameters=set[], requirements=set[Location Community1])
    let D = ComplexBid({demand with Id="D"}, parameters=set[], requirements=set[Generation Solar; Location Community1])
    let E = ComplexBid({demand with Id="E"}, parameters=set[], requirements=set[Location Community2])

    let bp = [A1;A2;B;C;D;E] |> Matching.assignToPools |> fst |> Seq.toList

    let bs = [
        [P2P("A1","B"); P2P("A1","C"); P2P("A1","D")]
        [P2P ("A2", "B")]
        [P2P ("A1", "B"); P2P ("A2", "B")]
        [P2P ("A1", "C")]
        [P2P ("A1", "D")]
        []
    ]

    testCase "Matches should equal" <| fun _ ->
        List.zip bp bs
        |> List.iter (fun ((b,pr),ps) -> 
            Expect.equal pr (Set.ofList ps) $"P2P pools should equal"
        )
