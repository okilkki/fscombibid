module Tests.Examples

open Expecto
open FsCombibid


[<Measure>] type kWh
[<Measure>] type EUR


type Commodity =
    | Electricity
    | Heat

type Parameter = 
    | Commodity of Commodity
    | Location of string


let bidIsClose clearing (bidId,(q,c)) =
    let result = bidCleared clearing bidId
    let isClose = Expect.floatClose Accuracy.high
    testCase $"Bid:'{bidId}' should match {(q,c)}" <| fun _ ->
        isClose (float result.Quantity) q $"Accepted quantity should be {q}"
        isClose (float result.Cost) c $"Accepted cost should be {c}"


let describeResults case clearing =
    printfn $"--- {case} ---"
    clearing.ClearedBids |>
        Map.iter (fun b r ->
            printf $"{b}: Quantity=% .1f{r.Quantity} kWh"
            printfn $"    Price=% .1f{r.Price} EUR/KWh"
        )


[<Tests>]
let explicitMarkets =
    let case = "ExplicitMarkets"

    // Define bids
    let a = {Id = "A"; Quantity = -1.0<kWh>; Cost = 3.0<EUR>}
    let b = {Id = "B"; Quantity = 2.0<kWh>; Cost = -4.0<EUR>}
    let c = {Id = "C"; Quantity = 2.0<kWh>; Cost = -3.0<EUR>}

    // Assign to markets
    let bids = [
        PoolBid(a, set[Market Electricity])
        PoolBid(b, set[Market Electricity; Market Heat])
        PoolBid(c, set[Market Heat])
    ]

    // Clear using default allocation and compensation (pay-as-bid)
    let clearing = Market.clear bids

    describeResults case clearing

    [
        "A", (-1.,  3.)
        "B", ( 1., -2.)
        "C", ( 0.,  0.)
    ]
    |> List.map (bidIsClose clearing)
    |> testList $"{case}.cleared"


[<Tests>]
let basicExample =
    let case = "BasicExample"

    // Define bids
    let a1 = PoolBid({Id= "A1"; Quantity= 1.<kWh>; Cost= -2.<EUR>}, set[Market Electricity])
    let a2 = PoolBid({Id= "A2"; Quantity= -1.<kWh>; Cost= 1.5<EUR>}, set[Market Heat])
    let b =  PoolBid({Id= "B"; Quantity= 2.<kWh>; Cost= -3.<EUR>}, set[Market Heat; Market Electricity])
    let c =  PoolBid({Id= "C"; Quantity= -1.<kWh>; Cost= 1.<EUR>}, set[Market Electricity])

    let bids = [
        Bundle(a1 &! a2) + 1.5<EUR>
        b; c
    ]

    // Clear using default allocation and compensation (pay-as-bid)
    let clearingSuccess = Market.clear bids

    describeResults case clearingSuccess

    let successTests = List.map (bidIsClose clearingSuccess) [
        "A1", (1.,  -2.)
        "A2", (-1., 1.5)
        "B", ( 1.,  -1.5)
        "C", (-1.,  1.)
    ]
    
    // If not enough valuation given, nothing traded
    let bids = [
        Bundle(a1 &! a2) + 0.5<EUR>
        b; c
    ]
    let failureTests =
        List.map ((fun id -> id, (0.,0.)) >> (bids |> Market.clear |> bidIsClose))["A1"; "A2"; "B"; "C"]

    [successTests; failureTests]
    |> List.concat
    |> testList $"{case}.cleared"

